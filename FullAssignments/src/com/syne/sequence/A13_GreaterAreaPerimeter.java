package com.syne.sequence;

import java.util.Scanner;

public class A13_GreaterAreaPerimeter {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter length of rectangle : ");
		float l = sc.nextFloat();
		System.out.println("Enter breadth of rectangle : ");
		float b = sc.nextFloat();
		
		float perimeter = 2.0f*(l+b);
		float area = l*b;
		
		if(area>perimeter)
			System.out.println("Area is "+area+ " which is greater than perimeter "+perimeter);
		else if(perimeter>area)
			System.out.println("Perimeter is "+perimeter+ " which is greater than area "+area);
		else
			System.out.println("Area is "+area+ " which is equal to "+perimeter);

	}

}
