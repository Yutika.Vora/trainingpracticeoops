package com.syne.sequence;

import java.util.Scanner;

public class A7_OddEven {

	public static void main(String[] args) {
		System.out.println("Enter number to check for odd/even : ");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		boolean result = n % 2 == 0 ? true:false;
		if(result)
			System.out.println(n+ " is even");
		else
			System.out.println(n+ " is odd");

	}

}
