package com.syne.sequence;

import java.util.Scanner;

public class A1_Swapping {

	public static void main(String[] args) {
		
		int temp;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number 1 : ");
		int a = sc.nextInt();
		System.out.println("Enter number 2 : ");
		int b = sc.nextInt();
		temp = a;
		a = b;
		b = temp;
		System.out.println("Now the first number is "+a+" and second number is "+b);
	}

}
