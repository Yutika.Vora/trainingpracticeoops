package com.syne.sequence;

import java.util.Scanner;

public class A15_PointInCircle 
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter center of circle coordinates : ");
		int cx = sc.nextInt();
		int cy = sc.nextInt();
		System.out.println("Enter radius of circle : ");
		int r = sc.nextInt();
		System.out.println("Enter coordinates of point  : ");
		int x = sc.nextInt();
		int y = sc.nextInt();
		
		if(Math.pow((x-cx), 2)+Math.pow((y-cy), 2) > Math.pow(r, 2))
			System.out.println("Point is Outside the circle");
		else if(Math.pow((x-cx), 2)+Math.pow((y-cy), 2) < Math.pow(r, 2))
			System.out.println("Point is Inside the circle");
		else
			System.out.println("Point is On the circle");
			
	}
}
