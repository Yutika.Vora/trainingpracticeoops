package com.syne.sequence;

import java.util.Scanner;

public class A5_EmployeeSalary {

	public static void main(String[] args) {
		
		double HRA,DA;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter employee basic salary : ");
		int bSalary = sc.nextInt();
		
		if(bSalary<1500)
		{
			HRA = 0.1*bSalary;
			DA = 0.9*bSalary;
		}		
		else
		{
			HRA = 500;
			DA = 0.98*bSalary;
		}
		System.out.println("Gross Salary is "+(bSalary + HRA + DA));
	}

}
