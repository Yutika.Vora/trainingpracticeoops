package com.syne.sequence;

import java.util.Scanner;

public class A12_AbsValue {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number to find absolute value : ");
		int n1 = sc.nextInt();
		if(n1<0)
			System.out.println("Absolute value is "+(-n1));
		else
			System.out.println("Absolute value is "+n1);
	}

}
