package com.syne.sequence;

import java.util.Scanner;

public class A21_PointInCartesian {

	public static void main(String[] args) {
		System.out.println("Enter the coordinates of the point : ");
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int y = sc.nextInt();
		if(x>0 &&y>0)
			System.out.println("Point ("+x+","+y+") lies in the first quadrant");
		if(x<0 &&y>0)
			System.out.println("Point ("+x+","+y+") lies in the second quadrant");
		if(x<0 &&y<0)
			System.out.println("Point ("+x+","+y+") lies in the third quadrant");
		if(x>0 &&y<0)
			System.out.println("Point ("+x+","+y+") lies in the fourth quadrant");
		if(x==0)
			if(y!=0)
				System.out.println("Point ("+x+","+y+") lies on y axis");
		if(y==0)
			if(x!=0)
				System.out.println("Point ("+x+","+y+") lies on x axis");
		if(x==0 && y==00)
			System.out.println("Point ("+x+","+y+") lies on origin");
	}

}
