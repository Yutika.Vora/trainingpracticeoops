package com.syne.sequence;

import java.util.Scanner;

public class A6_CpSp {

	public static void main(String[] args) {
		
		double cp,sp;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Cost Price : ");
		cp = sc.nextInt();
		System.out.println("Enter selling price : ");
		sp = sc.nextInt();
		
		if(sp>cp)
			System.out.println("Seller has made a profit of "+(sp-cp));
		else if(cp>sp)
			System.out.println("Seller has made a loss of "+(cp-sp));
		else
			System.out.println("Seller has neither made profit nor loss!");

	}

}
