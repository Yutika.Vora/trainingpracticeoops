package com.syne.sequence;

import java.util.Scanner;

public class A18_Percentage {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter marks for 5 subjects(Out of 100) : ");
		float m1 = sc.nextFloat();
		float m2 = sc.nextFloat();
		float m3 = sc.nextFloat();
		float m4 = sc.nextFloat();
		float m5 = sc.nextFloat();
		
		float p1 = (m1+m2+m3+m4+m5)/5;
		
		if(p1>=60)
			System.out.println("First Division");
		else if(p1<60 && p1>=50)
			System.out.println("Second Division");
		else if(p1<50 && p1>=40)
			System.out.println("Third Division");
		else
			System.out.println("Fail");
	}

}
