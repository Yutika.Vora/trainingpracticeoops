package com.syne.sequence;

import java.util.Scanner;

public class A3_Discount {

	public static void main(String[] args) {
		
		double total;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter quantity : ");
		int quantity = sc.nextInt();
		System.out.println("Enter price : ");
		double price = sc.nextDouble();
		
		if(quantity>1000)
			total = (price*quantity)-(0.1*(price*quantity));
		else
			total = price*quantity;
		System.out.println("Total expenses = "+total);
	}

}
