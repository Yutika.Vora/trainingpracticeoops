package com.syne.sequence;

import java.util.Scanner;

public class A19_Lessthan0 {

	public static void main(String[] args) 
	{
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number : ");
		int m = sc.nextInt();
		
		if(m<0)
			n=-1;
		else if(m>0)
			n=1;
		else
			n=0;
		
		System.out.println("Value of n = "+n);
	}

}
