package com.syne.sequence;

import java.util.Scanner;

public class A22_CharacterCase
{

	public static void main(String[] args) {
		System.out.println("Enter a character : ");
		Scanner sc = new Scanner(System.in);
		char a = sc.next().charAt(0);
		if (a>=65 && a<=90)
            System.out.println("\n" + a +" is an UpperCase character");
     
        else if (a>= 97 && a<= 122)
            System.out.println("\n" + a +"is an LowerCase character" );
     
        else if (a>=48 && a<=57)
            System.out.println("\n" + a +" is a number" );
		
        else if (a>=0 && a<=47 || a>=58 && a<=64 || a>=91 && a<=96 || a>=123 && a<=125)
            System.out.println("\n" + a +" is a special character" );
	}

}


