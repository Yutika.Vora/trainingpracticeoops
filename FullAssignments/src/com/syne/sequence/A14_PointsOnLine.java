package com.syne.sequence;

import java.util.Scanner;

public class A14_PointsOnLine {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter coordinates of point 1 : ");
		int x1 = sc.nextInt();
		int y1 = sc.nextInt();
		System.out.println("Enter coordinates of point 2 : ");
		int x2 = sc.nextInt();
		int y2 = sc.nextInt();
		System.out.println("Enter coordinates of point 3 : ");
		int x3 = sc.nextInt();
		int y3 = sc.nextInt();
		
		if ((y3 - y2) * (x2 - x1) ==
		        (y2 - y1) * (x3 - x2))
		        System.out.println("The points are on same line");
		    else
		        System.out.println("The points are not on same line.");
	}

}
