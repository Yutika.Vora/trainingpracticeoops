package com.syne.sequence;

import java.util.Scanner;

public class A2_distanceConversion {

	public static void main(String[] args) {

		double m, cm, mm;
		System.out.println("Enter the distance in kilometer : ");
		Scanner sc = new Scanner(System.in);
		double km = sc.nextInt();
		m = 1000*km;
		cm = 100*m;
		mm = 10*cm;
		System.out.println("Kilometers ="+km+"\nMeters="+m+"\nCantimeters="+cm+"\nMilimeters="+mm);
		
	}

}
