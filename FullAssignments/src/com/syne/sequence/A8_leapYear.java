package com.syne.sequence;

import java.util.Scanner;

public class A8_leapYear {

	public static void main(String[] args) {
		
		System.out.println("Enter year to check if leap year or not : ");
		Scanner sc = new Scanner(System.in);
		int year = sc.nextInt();

		if(year%4==0)
			System.out.println("This year is leap year.");
		else
			System.out.println("This year is not a leap year.");
	}

}
