package com.syne.sequence;

import java.util.Scanner;

public class A20_Height {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the height of person in cm  : ");
		int h = sc.nextInt();
		
		if(h<150)
			System.out.println("Person is short");
		else if(h>150 && h<165)
			System.out.println("Person has average height");
		else if(h>165 && h<195)
			System.out.println("Person is tall");
		else
			System.out.println("Person has abnormal height");
	}

}
