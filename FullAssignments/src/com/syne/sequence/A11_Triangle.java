package com.syne.sequence;

import java.util.Scanner;

public class A11_Triangle {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter angle 1 for triangle : ");
		int n1 = sc.nextInt();
		System.out.println("Enter angle 2 for triangle : ");
		int n2 = sc.nextInt();
		System.out.println("Enter angle 3 for triangle : ");
		int n3 = sc.nextInt();
		
		if(n1+n2+n3==180)
			System.out.println("Triangle is valid");
		else
			System.out.println("Triangle is invalid");
	}

}
