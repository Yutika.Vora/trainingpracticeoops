package com.syne.sequence;

import java.util.Scanner;

public class A16_PointLiesOnXY {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter coordinates of point  : ");
		int x = sc.nextInt();
		int y = sc.nextInt();
		
		if(x==0 && y!=0)
			System.out.println("Point is on Y-Axis");
		else if(y==0 && x!=0)
			System.out.println("Point is on X-axis");
		else if(x==0 && y==0)
			System.out.println("Point is On origin");
		else
			System.out.println("Point is neither on x-axis, y-axis or origin.");
	}

}
